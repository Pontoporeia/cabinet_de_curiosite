var main = document.getElementById("main_container");
var ww = window.innerWidth;
var h = window.innerHeight;

var isHome = document.querySelector('.home') ? true : false;
var titreHome = isHome ? document.getElementById("title") : '';
var title = isHome ? document.getElementById("titre").getBoundingClientRect() : '';
var titleObject = !isHome ? document.getElementById('titleObject') : '';
var titleObjectH = titleObject.clientHeight;
var synopsis = !isHome ? document.querySelector('.synopsis') : '';
var object = !isHome ? document.querySelector('.object') : '';

var w = window.innerWidth / 12; // /7 parcequ'il y a 7 typo diff donc 7 zones
var h = window.innerHeight;

var xt = title.left;
var yt = title.top;

var wt = titreHome.clientWidth;
var ht = titreHome.clientHeight;


window.onload = function () {

  xt = Math.round(xt + wt / 2);
  yt = Math.round(yt + ht / 2);

  if (ww > 1000) {
    !isHome ? synopsis.style.height = h - titleObjectH - 10 + 'px' : '';
    !isHome ? object.style.height = h - titleObjectH - 10 + 'px' : '';
  }

};


window.onresize = function () {
  w = window.innerWidth / 12; // /7 parcequ'il y a 7 typo diff donc 7 zones
  h = window.innerHeight;

  xt = Math.round(xt + wt / 2);
  yt = Math.round(yt + ht / 2);

  if (w > 1000) {
    !isHome ? synopsis.style.height = h - titleObjectH - 10 + 'px' : '';
    !isHome ? object.style.height = h - titleObjectH - 10 + 'px' : '';
  }

}

if (isHome && ww > 1000) {
  document.addEventListener("mousemove", function (e) {
    titleChange(e);
    movement(e);
  });
}

function movement(event) {
  var mouseX = event.clientX;
  var mouseY = event.clientY;

  main.style.left = ww - mouseX - 100 + "px";
  main.style.top = h - mouseY - 100 + "px";
}

function titleChange(event) {
  var mouseX = event.clientX;
  var mouseY = event.clientY;

  var distX = xt - mouseX;
  if (distX < 0) {
    distX = -distX;
  }

  var distY = yt - mouseY;
  if (distY < 0) {
    distY = -distY;
  }
  titreHome.className = '';
  if (distX < w / 2) {
    titreHome.classList.add('redaction');
  } else if (distX < w && distX > w / 2) {
    titreHome.classList.add('redaction10');
  } else if (distX < w * 2 && distX > w) {
    titreHome.classList.add('redaction20');
  } else if (distX < w * 3 && distX > w * 2) {
    titreHome.classList.add('redaction35');
  } else if (distX < w * 4 && distX > w * 3) {
    titreHome.classList.add('redaction50');
  } else if (distX < w * 5 && distX > w * 4) {
    titreHome.classList.add('redaction70');
  } else {
    titreHome.classList.add('redaction100');
  }
}